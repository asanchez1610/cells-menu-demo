import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-menu-demo.js';

suite('CellsMenuDemo', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-menu-demo></cells-menu-demo>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});
