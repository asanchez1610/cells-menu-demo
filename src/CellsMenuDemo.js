import { LitElement, html } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsMenuDemo-styles.js';
import { bbvaMenu, bbvaOnoff } from '@bbva-web-components/bbva-foundations-icons';
import '@bbva-web-components/bbva-core-icon/bbva-core-icon.js';

/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-menu-demo></cells-menu-demo>
```

##styling-doc

@customElement cells-menu-demo
*/
export class CellsMenuDemo extends LitElement {
  static get is() {
    return 'cells-menu-demo';
  }

  // Declare properties
  static get properties() {
    return {
      titleApp: String,
      options: Array,
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.titleApp = 'My App';
    this.options = [];
    window.addEventListener('set-current-active-page', ({detail}) => this.setCurrentpageActive(detail));
  }

  static get styles() {
    return [styles, getComponentSharedStyles('cells-menu-demo-shared-styles')];
  }

  showMenuResponsive() {
    let topNav = this.shadowRoot.querySelector('#myTopnav');
    if (topNav.className === 'topnav') {
      topNav.className += ' responsive';
    } else {
      topNav.className = 'topnav';
    }
  }

  clearOptionsSelected() {
    const options = this.shadowRoot.querySelectorAll('.option-item');
    options.forEach((option) => {
      option.classList.remove('active');
    });
  }

  selectedOptionItem({ target }) {
    this.clearOptionsSelected();
    target.classList.add('active');
  }

  setCurrentpageActive(id) {
    console.log('setCurrentpageActive', id);
    this.clearOptionsSelected();
    this.shadowRoot.querySelector(`#${id}_option`).classList.add('active');
  }

  logOut() {
    const logOutEvent = new CustomEvent('log-out-event', { 
      detail: true,
      bubbles: true, 
      composed: true
    });
    this.dispatchEvent(logOutEvent);
  }

  // Define a template
  render() {
    return html`
      <slot></slot>
      <header class="topnav" id="myTopnav">
        <a class="title">${this.titleApp}</a>
        <a
                id="out-session_option"
                href="#"
                class="option-item session-out"
                @click="${this.logOut}"
                >Cerrar Sesion</a
              >
        ${this.options.map(
          (item) =>
            html`
              <a
                id="${item.name}_option"
                href="${item.page ? item.page : '#'}"
                class="option-item"
                @click="${this.selectedOptionItem}"
                >${item.title}</a
              >
            `
        )}
        <a class="logout" @click="${this.logOut}">
          <bbva-core-icon icon="${bbvaOnoff()}"></bbva-core-icon>
        </a>
        <a class="icon" @click="${this.showMenuResponsive}">
          <bbva-core-icon icon="${bbvaMenu()}"></bbva-core-icon>
        </a>
      </header>
    `;
  }
}
