/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box;
  --bg-color: #072146;
  --bg-color-title: #006C6C;
  --bg-color-active-option: #004481;
  --bg-color-hover-option: #004481;
  --text-color-option: #f2f2f2;
  --text-color-option-hover: #ffffff;
  --text-color-option-active: #ffffff;
}

:host([hidden]), [hidden] {
  display: none !important;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.topnav {
  background-color: var(--bg-color);
  position: fixed;
  width: 100%;
  top: 0;
  left: 0;
  overflow: hidden;
  z-index: 10;
}

.topnav a {
  float: left;
  display: block;
  color: var(--text-color-option);
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  font-weight: lighter;
}

.topnav a:hover {
  background-color: var(--bg-color-hover-option);
  color: var(--text-color-option-hover);
}

.topnav a.active {
  background-color: var(--bg-color-active-option);
  color: var(--text-color-option-active);
}

.topnav .icon {
  display: none;
}

a.logout {
  float: right;
  display: block;
  padding-bottom: 11px;
  cursor: pointer;
}

a.session-out {
  display: none !important;
}

a.session-out:hover {
  background-color: #693c3c;
}

a.logout:hover {
  background-color: transparent !important;
}

a.icon:hover {
  background-color: transparent !important;
}

.title {
  background-color: var(--bg-color-title);
  font-weight: 500;
}

.title:hover {
  background-color: var(--bg-color-title) !important;
  color: var(--text-color-option) !important;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {
    display: none;
  }

  .topnav a.icon {
    float: right;
    display: block;
    padding-bottom: 11px;
    cursor: pointer;
  }

  a.logout {
    display: none !important;
  }
}
@media screen and (max-width: 600px) {
  .topnav.responsive {
    position: fixed;
    z-index: 100;
  }

  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }

  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }

  .topnav.responsive a.session-out {
    display: block !important;
  }
}
`;